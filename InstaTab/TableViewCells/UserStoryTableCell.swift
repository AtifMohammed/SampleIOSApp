//
//  UserStoryTableCell.swift
//  InstaTab
//
//  Created by Mohammed Atif on 06/01/18.
//  Copyright © 2018 Mohammed Atif. All rights reserved.
//

import UIKit
import Alamofire

class UserStoryTableCell : UITableViewCell {
    
    //MARK: UIViews
    
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var uploadedImage: UIImageView!
    @IBOutlet weak var storyCaption: UILabel!
    @IBOutlet weak var postLikes: UILabel!
    @IBOutlet weak var totalViews: UILabel!
    
    //MARK: Buttons
    
    @IBOutlet weak var moreOptions: UIButton!
    @IBOutlet weak var favorite: UIButton!
    @IBOutlet weak var comment: UIButton!
    @IBOutlet weak var share: UIButton!
    
    //MARK: Constraints

    @IBOutlet weak var heightRatioConstraint: NSLayoutConstraint!
    
    //MARK: Private Methods
    
    func updateViews(width : CGFloat, height : CGFloat, imageUrl : String, avatarUrl : String){
        userAvatar.layer.cornerRadius = 24
        userAvatar.layer.masksToBounds = true
        let viewRatio = width / height
        let newHeightConstraint = NSLayoutConstraint(
            item: heightRatioConstraint.firstItem as Any,
            attribute: heightRatioConstraint.firstAttribute,
            relatedBy: heightRatioConstraint.relation,
            toItem: heightRatioConstraint.secondItem,
            attribute: heightRatioConstraint.secondAttribute ,
            multiplier: viewRatio,
            constant: heightRatioConstraint.constant)
        uploadedImage.removeConstraint(heightRatioConstraint)
        uploadedImage.addConstraint(newHeightConstraint)
        heightRatioConstraint = newHeightConstraint
        uploadedImage.imageFromUrl(urlString: imageUrl)
        userAvatar.imageFromUrl(urlString: avatarUrl)
    }
}

extension UIImageView {
    public func imageFromUrl(urlString: String) {
        self.image = nil
        Alamofire.request(urlString).responseData { (response) in
            if response.error == nil {
                print(response.result)
                // Show the downloaded image:
                if let data = response.data {
                    self.image = UIImage(data: data)
                }
            }
        }
    }
}
